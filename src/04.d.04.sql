USE tienda;

SELECT *
FROM producto
WHERE 1=1
AND codigo_fabricante = (SELECT codigo FROM fabricante WHERE nombre = 'ASUS')
AND precio > (SELECT AVG(precio) FROM producto WHERE codigo_fabricante IN (SELECT codigo FROM fabricante WHERE nombre = 'ASUS'))
