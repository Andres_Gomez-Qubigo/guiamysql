USE personal;

SELECT nombre,
       LENGTH(nombre) AS 'lONGITUD'
FROM empleados
WHERE 1 = 1
  AND LENGTH(nombre) >= 11
