USE nba;

-- PROMEDIO PUNTOS POR PARTIDO POR EQUIPO
SELECT AVG(puntos_local + puntos_visitante) / 2 AS 'Media'
FROM partidos AS P
JOIN equipos e on e.Nombre = P.equipo_local
WHERE E.Division = 'PACIFIC';

-- PROMEDIO PUNTOS POR PARTIDO AMBOS EQUIPOS
SELECT AVG(puntos_local + puntos_visitante) AS 'Media'
FROM partidos AS P
JOIN equipos e on e.Nombre = P.equipo_local
WHERE E.Division = 'PACIFIC';
