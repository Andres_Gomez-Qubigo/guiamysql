USE nba;

SELECT SUM(E.Puntos_por_partido) AS 'Suma'
FROM estadisticas AS E
         JOIN jugadores j on j.codigo = E.jugador
         JOIN equipos e2 on e2.Nombre = j.Nombre_equipo
WHERE 1 = 1
  AND J.Procedencia = 'SPAIN'
  AND E2.Ciudad = 'LOS ANGELES'
