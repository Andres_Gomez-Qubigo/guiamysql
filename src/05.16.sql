USE nba;

SELECT equipo_local,
       equipo_visitante,
       ABS(puntos_visitante - puntos_local) AS Diferencia
FROM partidos
WHERE ABS(puntos_visitante - puntos_local) = (SELECT MAX(ABS(puntos_local - puntos_visitante)) FROM partidos)
