USE personal;

SELECT nombre
FROM empleados
WHERE 1 = 1
  AND idDepto NOT IN
      (SELECT departamentos.idDepto FROM departamentos WHERE nombre NOT IN ('VENTAS', 'INVESTIGACION', 'MANTENIMIENTO'))
