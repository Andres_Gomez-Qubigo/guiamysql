USE personal;

SELECT MAX(salEmp)                 AS 'MAXS',
       MIN(salEmp)                 AS 'MINS',
       (MAX(salEmp) - MIN(salEmp)) AS 'DIF'
FROM empleados
