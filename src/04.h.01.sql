USE tienda;

SELECT F.nombre
FROM producto AS P
         JOIN fabricante f on f.codigo = P.codigo_fabricante
WHERE F.nombre != 'LENOVO'
GROUP BY P.codigo_fabricante
HAVING COUNT(*) = (SELECT COUNT(*)
                   FROM producto AS P2
                            JOIN fabricante f2 on f2.codigo = P2.codigo_fabricante
                   WHERE codigo_fabricante = 2)
