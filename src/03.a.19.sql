USE personal;

SELECT e.nombre,
       d.nombreDpto,
       e.cargoE,
       e.salEmp,
       e.fecIncorporacion
FROM empleados AS e
         JOIN departamentos d on d.idDepto = e.idDepto
WHERE 1 = 1
  AND cargoE IN ('SECRETARIA', 'VENDEDOR')
  AND d.nombreDpto != 'PRODUCCION'
  AND e.salEmp > 1000
ORDER BY fecIncorporacion
