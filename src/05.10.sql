USE nba;

SELECT J.Nombre,
       SUM(Puntos_por_partido)
FROM estadisticas AS E
         JOIN jugadores j on j.codigo = E.jugador
GROUP BY E.jugador, J.Nombre
ORDER BY J.Nombre
