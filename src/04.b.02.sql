USE tienda;

SELECT P.nombre,
       P.precio,
       F.nombre
FROM producto AS P
JOIN fabricante f on f.codigo = P.codigo_fabricante
ORDER BY F.nombre
