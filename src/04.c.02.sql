USE tienda;

SELECT F.nombre
FROM fabricante AS F
LEFT JOIN producto p on F.codigo = p.codigo_fabricante
WHERE P.codigo_fabricante IS NULL
