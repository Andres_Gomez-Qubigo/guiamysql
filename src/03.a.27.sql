USE personal;

SELECT AVG(e.salEmp) AS 'Promedio por depto',
       d.nombreDpto
FROM empleados AS e
         JOIN departamentos d on d.idDepto = e.idDepto
GROUP BY d.nombreDpto
ORDER BY d.nombreDpto;
