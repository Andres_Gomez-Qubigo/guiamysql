USE personal;

SELECT salEmp,
       comisionE
FROM empleados
WHERE 1 = 1
  AND idDepto = 2000
ORDER BY comisionE
