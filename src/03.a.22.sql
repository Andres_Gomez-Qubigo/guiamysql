USE personal;

SELECT *
FROM empleados
WHERE 1 = 1
  AND nombre LIKE 'M%'
  AND (salEmp > 800 OR comisionE > 0)
  AND idDepto IN (SELECT departamentos.idDepto FROM departamentos WHERE nombreDpto = 'VENTAS')
