USE personal;

SELECT comisionE,
       (salEmp * 0.3) AS '30% del sueldo',
       nombre
FROM empleados
WHERE comisionE <= (salEmp * 0.3)
