USE personal;

SELECT MAX(MYSUM), MYNAME
FROM (
         SELECT SUM(salEmp) AS MYSUM,
                D.nombreDpto AS MYNAME
         FROM empleados AS E
                  JOIN departamentos D on D.idDepto = E.idDepto
         GROUP BY D.nombreDpto
    ORDER BY sum(salEmp) DESC
     ) as MnD
GROUP BY MYSUM;

SELECT MIN(MYSUM), MYNAME FROM (
               SELECT SUM(salEmp) AS MYSUM, D.nombreDpto AS MYNAME
               FROM
               empleados AS E
               JOIN departamentos d on d.idDepto = E.idDepto
               GROUP BY D.nombreDpto
    ORDER BY SUM(MYSUM) DESC
           ) as EdM
