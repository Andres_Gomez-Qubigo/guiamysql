USE tienda;

SELECT P.codigo,
       P.nombre,
       P.codigo_fabricante,
       F.nombre
FROM producto AS p
JOIN fabricante f on f.codigo = p.codigo_fabricante
