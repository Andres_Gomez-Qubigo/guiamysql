USE nba;

-- PUNTOS TOTALES DE CADA EQUIPO, TANTO DE LOCAL COMO DE VISITANTE
SELECT Equipo, SUM(Total)
FROM (SELECT equipo_local      AS Equipo,
             SUM(puntos_local) AS Total
      FROM partidos
      GROUP BY Equipo
      UNION
      SELECT equipo_visitante      AS Equipo,
             SUM(puntos_visitante) AS Total
      FROM partidos
      GROUP BY Equipo) AS TEMP
GROUP BY Equipo;

-- PUNTOS DE CADA EQUIPO POR PARTIDO, TANTO DE LOCAL COMO VISITANTE
SELECT codigo,
       equipo_local AS Equipo,
       puntos_local AS Puntos
FROM partidos
UNION ALL
SELECT codigo,
       equipo_visitante AS Equipo,
       puntos_visitante AS Puntos
FROM partidos
ORDER BY Equipo, codigo;

-- PUNTOS POR PARTIDO
SELECT equipo_local,
       equipo_visitante,
       puntos_local,
       puntos_visitante
FROM partidos
ORDER BY equipo_local, equipo_visitante