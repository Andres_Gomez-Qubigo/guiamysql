USE personal;

SELECT d.nombreDpto,
       COUNT(*) AS 'Cantidad de empleados'
FROM empleados AS e
         INNER JOIN departamentos d on e.idDepto = d.idDepto
GROUP BY d.nombreDpto
ORDER BY d.nombreDpto
