USE nba;

SELECT E.Nombre,
       E.Conferencia,
       E.Division
FROM jugadores AS J
JOIN equipos e on e.Nombre = J.Nombre_equipo
ORDER BY J.Altura DESC
LIMIT 1
