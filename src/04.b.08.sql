USE tienda;

SELECT P.nombre,
       P.precio,
       F.nombre
FROM producto AS P
JOIN fabricante f on f.codigo = P.codigo_fabricante
WHERE 1=1
AND P.precio <= 180
ORDER BY P.precio DESC, P.nombre
