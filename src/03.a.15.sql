USE personal;

SELECT
salEmp,
       comisionE,
       (salEmp + comisionE) AS 'Salario Total',
       nombre
FROM empleados
WHERE 1=1
AND comisionE > 1000
