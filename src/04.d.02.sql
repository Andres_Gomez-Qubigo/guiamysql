USE tienda;

SELECT *
FROM producto
WHERE 1=1
AND precio = (SELECT MAX(precio) FROM producto WHERE codigo_fabricante = 2)
AND codigo_fabricante != 2
