USE tienda;

SELECT P.nombre,
       MIN(P.precio),
       F.nombre
FROM producto AS P
JOIN fabricante f on f.codigo = P.codigo_fabricante
