USE nba;

SELECT J.Nombre,
       SUM(E.Puntos_por_partido) AS 'Total'
FROM estadisticas AS E
JOIN jugadores j on j.codigo = E.jugador
GROUP BY E.jugador
ORDER BY Total DESC
LIMIT 1
