USE personal;

SELECT e.nombre,
       e.salEmp,
       d.nombreDpto
FROM empleados AS e
         LEFT JOIN departamentos d on d.idDepto = e.idDepto
WHERE salEmp >= (SELECT AVG(salEmp) FROM empleados)
ORDER BY d.nombreDpto
