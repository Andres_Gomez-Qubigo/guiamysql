USE personal;

SELECT nombre,
       cargoE
FROM empleados
WHERE 1=1
AND nombre RLIKE '^[j-z]'
ORDER BY nombre, cargoE
