USE nba;

SELECT codigo,
       equipo_local,
       equipo_visitante,
       CASE
           WHEN puntos_local > puntos_visitante THEN equipo_local
           WHEN puntos_local < puntos_visitante THEN equipo_visitante
           WHEN puntos_local = puntos_visitante THEN NULL
           END AS Ganador
FROM partidos
ORDER BY codigo
