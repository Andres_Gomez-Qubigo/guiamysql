USE personal;

SELECT COUNT(codJefe),
       e.codJefe,
       e.nombre,
       d.nombreDpto,
       e.cargoE
FROM empleados AS e
         LEFT JOIN departamentos d on d.idDepto = e.idDepto
WHERE 1 = 1
  -- AND cargoE LIKE '%jefe%'
  AND e.codJefe IS NOT NULL
GROUP BY e.codJefe, e.cargoE
having COUNT(codJefe) >= 2
   and cargoE NOT LIKE '%JEFE%'
;


SELECT COUNT(E.codJefe),
       E.codJefe,
       E.idDepto,
       d.nombreDpto
FROM empleados AS E
         JOIN departamentos d on d.idDepto = E.idDepto
WHERE E.codJefe IS NOT NULL
  AND E.cargoE NOT LIKE '%JEFE%'
GROUP BY E.codJefe
ORDER BY E.codJefe;


select
count(codJefe), d.nombreDpto
from empleados
join departamentos d on d.idDepto = empleados.idDepto
group by codJefe
having count(*) > 1;


select distinct codJefe from empleados
